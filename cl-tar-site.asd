;;; -*- mode: lisp -*-

(asdf:defsystem #:cl-tar-site
  :version "0.0.1"
  :author "Eric Timmons <eric@timmons.dev>"
  :description "Website for cl-tar project."
  :license "MIT"
  :depends-on ("40ants-doc-full" "40ants-doc")
  :pathname "src/"
  :components ((:file "package")
               (:file "utils" :depends-on ("package"))
               (:file "cl-tar-site" :depends-on ("package" "utils"))))
