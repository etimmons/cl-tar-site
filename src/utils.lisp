;;; This file is part of cl-tar-site. See LICENSE and README.md for more
;;; information.

(in-package #:cl-tar-site)

(defun cl-tar-file-versions ()
  (let ((all-versions (uiop:subdirectories (asdf:system-relative-pathname
                                            :cl-tar-site "cl-tar-file/"))))
    (setf all-versions (mapcar (lambda (x)
                                 (car (last (pathname-directory x))))
                               all-versions))
    (setf all-versions (mapcar (lambda (x)
                                 (if (probe-file (merge-pathnames
                                                  "manual.html"
                                                  (merge-pathnames (uiop:ensure-directory-pathname x)
                                                                   (asdf:system-relative-pathname
                                                                    :cl-tar-site "cl-tar-file/"))))
                                     (list x (uiop:strcat x "/manual.html"))
                                     (list x (uiop:strcat x "/manual/"))))
                               all-versions))
    (nreverse
     (sort all-versions #'uiop:version<
           :key (lambda (x) (uiop:parse-version (subseq (first x) 1)))))))

(defun cl-tar-versions ()
  (let ((all-versions (uiop:subdirectories (asdf:system-relative-pathname
                                            :cl-tar-site "cl-tar/"))))
    (setf all-versions (mapcar (lambda (x)
                                 (car (last (pathname-directory x))))
                               all-versions))
    (setf all-versions (mapcar (lambda (x)
                                 (if (probe-file (merge-pathnames
                                                  "manual.html"
                                                  (merge-pathnames (uiop:ensure-directory-pathname x)
                                                                   (asdf:system-relative-pathname
                                                                    :cl-tar-site "cl-tar/"))))
                                     (list x (uiop:strcat x "/manual.html"))
                                     (list x (uiop:strcat x "/manual/"))))
                               all-versions))
    (nreverse
     (sort all-versions #'uiop:version<
           :key (lambda (x) (uiop:parse-version (subseq (first x) 1)))))))
