;;; This file is part of cl-tar-site. See LICENSE and README.md for more
;;; information.

(in-package #:cl-tar-site)

(defsection @cl-tar-site (:title "CL-TAR Project")
  "The CL-TAR project works on making tar files readable and writeable directly
from Common Lisp, without shelling out to a tar executable. There are two sub
projects under the CL-TAR umbrella.

The first is
[cl-tar-file](https://gitlab.common-lisp.net/cl-tar/cl-tar-file). It defines a
single ASDF system: `tar-file`. This project focuses on reading and writing
physical tar file entries. It is a rather low-level library and it is expected
that not many projects will use it directly.

The second is [cl-tar](https://gitlab.common-lisp.net/cl-tar/cl-tar). It
defines several ASDF systems meant for use by other developers: `tar`,
`tar-simple-extract`, and `tar-extract`. This project is a high level interface
built on top of `cl-tar-file`. It reads and writes logical entries from/to tar
files. Additionally, it contains code to interface with your file system."

  "## Quickstart

You likely want to use `cl-tar` instead of `cl-tar-file` directly. Load it with:

```common-lisp
(asdf:load-system \"tar\")
```

If you want to extract a tar archive on any CL implementation, without
preserving all the metadata or symbolic links, run the following:

```common-lisp
(asdf:load-system \"tar-simple-extract\")

(tar:with-open-archive (a \"/path/to/file.tar\")
  (tar-simple-extract:simple-extract-archive a :directory #p\"/path/to/extraction/point/\"))
```

If you want to extract a tar archive, attempting to preserve symbolic links and
as much metadata as possible, evaluate the following. Note that this requires
osicat to work on your OS and implementation.

```common-lisp
(asdf:load-system \"tar-extract\")

(tar:with-open-archive (a \"/path/to/file.tar\")
  (tar-extract:extract-archive a :directory #p\"/path/to/extraction/point/\"))
```
"
  "## cl-tar

The cl-tar repository is located at <https://gitlab.common-lisp.net/cl-tar/cl-tar>"

  #.(let ((versions (cl-tar-versions)))
      (format nil "The latest `cl-tar` release is ~A, and the manual is [here](/cl-tar/~A)" (first (first versions)) (second (first versions))))

  "The manuals for all releases of `cl-tar` can be found below:"

  #.(let ((versions (cl-tar-versions)))
      (format nil "~{~{+ [~A](/cl-tar/~A)~}~^~%~}"
              versions))

  "## cl-tar-file

The cl-tar-file repository is located at <https://gitlab.common-lisp.net/cl-tar/cl-tar-file>"

  #.(let ((versions (cl-tar-file-versions)))
      (format nil "The latest `cl-tar-file` release is ~A, and the manual is [here](/cl-tar-file/~A)" (first (first versions)) (second (first versions))))

  "The manuals for all releases of `cl-tar-file` can be found below:"

  #.(let ((versions (cl-tar-file-versions)))
      (format nil "~{~{+ [~A](/cl-tar-file/~A)~}~^~%~}"
              versions)))


(defun generate-docs ()
  (40ants-doc/builder:update-asdf-system-docs @cl-tar-site :cl-tar-site :clean-urls nil))
